'use strict';

var app = angular.module('train.service', ['restangular', 'train.model', 'service']);

app.factory('trainService', function(Restangular, BaseService, Train) {

  var TrainService = BaseService.extend({
    init: function() {
      this._super(Train, Restangular);
    }

  });

  return new TrainService();

});
