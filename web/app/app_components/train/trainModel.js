'use strict';

var app = angular.module('train.model', ['model']);

app.factory('Train', function(BaseModel) {

  var Train = BaseModel.extend({
    init: function(data) {
      this._super(data);
    },

    getId: function() {
      return (!this.isNew()) ? this._links.self.href.substring(this._links.self.href.lastIndexOf('/') + 1) : undefined;
    }

  });

  Train.route = 'trains';

  return Train;
});
