'use strict';

var app = angular.module('user.service', ['restangular', 'user.model', 'service']);

app.factory('userService', function(Restangular, BaseService, User) {

  var UserService = BaseService.extend({
    init: function() {
      this._super(User, Restangular);
    }

  });

  return new UserService();

});
