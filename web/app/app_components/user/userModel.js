'use strict';

var app = angular.module('user.model', ['model']);

app.factory('User', function(BaseModel) {

  var User = BaseModel.extend({
    init: function(data) {
      this._super(data);
    },

    getId: function() {
      return (!this.isNew()) ? this._links.self.href.substring(this._links.self.href.lastIndexOf('/') + 1) : undefined;
    },

    getName: function() {
      return this.firstName + ' ' + this.lastName;
    }

  });

  User.route = 'users';

  return User;
});
