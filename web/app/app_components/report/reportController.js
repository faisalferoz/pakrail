'use strict';

var app = angular.module('report', ['seatbookingHistory.service', 'report.routes', 'user.service', 'page-header', 'ui.router', 'ui.select', 'xtForm', 'ngTable', 'sdeutils', 'ui.bootstrap', 'angular-growl', 'app.constants', 'underscore']);

app.controller('ReportController', function($scope, $filter, ngTableParams, sdeutils, growl, $state, userService, seatbookingHistoryService, PAGE, _) {

  // Clear the form and make a REST call to populate the list of jobs and other form lookup elements
  function init() {
    $scope.model = {
      user: ''
    };
    initializeUsers();
  }

  function initializeUsers() {
    userService.findAll({
      size: 1000
    }).then(function(users) {
      $scope.userList = users;
    });
  }

  $scope.generateReport = function() {
    if ($scope.model.user) {
      $scope.model.reportUser = $scope.model.user;
      $scope.model.totalFare = 0;
      var userId = $scope.model.user.getId();
      seatbookingHistoryService.findByCreatedById(userId).then(function(seatbookings) {
        $scope.model.totalFare = _.reduce(seatbookings, function(memo, booking) {
          return memo + booking.amount + booking.luggageAmount;
        }, 0);
        $scope.seatbookings = seatbookings;
        $scope.totalElements = seatbookings.page.totalElements;
        $scope.tableParams.reload();
      }, function(response) {
        if (typeof(response.data) === 'object') {
          growl.error('Error in training model. <br/> Error Message: ' + response.data.message);
        } else {
          growl.error('Error occurred while generating report.');
        }
      });
    }
  };

  $scope.tableParams = new ngTableParams({ // jshint ignore:line
    // show first page
    page: 1,
    // count per page
    count: PAGE.DEFAULT,
    // initial filter
    filter: {
      //name: 'M'
    },
    // initial sorting
    sorting: {
      id: 'asc'
    }
  }, {
    // length of data
    total: 0,
    counts: PAGE.COUNTS,
    itemLabel: 'Seat Bookings',
    getData: function($defer, params) {

      if ($scope.seatbookings === undefined || $scope.seatbookings === null) {
        return;
      }

      sdeutils.clearFilter($scope.tableParams.$params.filter);

      var seatbookings = $scope.seatbookings;
      var filteredData = params.filter() ? $filter('filter')(seatbookings, params.filter()) : seatbookings;
      var orderedData = params.sorting() ? $filter('orderBy')(filteredData, params.orderBy()) : filteredData;

      // set total for recalc pagination
      params.total(orderedData.length);

      // This is to take pagination back to page 1 if our filter is returning less than one page of data
      if (params.total() < (params.page() - 1) * params.count()) {
        params.page(1);
      }
      $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
    }
  });

  init();
});
