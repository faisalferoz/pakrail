'use strict';

var app = angular.module('seatbookingHistory.service', ['restangular', 'seatbookingHistory.model', 'service']);

app.factory('seatbookingHistoryService', function(Restangular, BaseService, SeatbookingHistory) {

  var SeatbookingHistoryService = BaseService.extend({

    init: function() {
      this._super(SeatbookingHistory, Restangular);
    },

    findByCreatedById: function(userId) {
      return this.future(this.Restangular.all(this.route).customGETLIST('/search/findByCreatedById', {
        userId: userId,
        size: 1000
      }));
    }

  });

  return new SeatbookingHistoryService();
});
