'use strict';

var app = angular.module('seatbookingHistory.model', ['model']);

app.factory('SeatbookingHistory', function(BaseModel) {

  var SeatbookingHistory = BaseModel.extend({
    init: function(data) {
      this._super(data);
    },

    getId: function() {
      return (!this.isNew()) ? this._links.self.href.substring(this._links.self.href.lastIndexOf('/') + 1) : undefined;
    }

  });

  SeatbookingHistory.route = 'seatBookingHistories';

  return SeatbookingHistory;
});
