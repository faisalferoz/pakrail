'use strict';

var app = angular.module('report.routes', ['ui.router']);

app.config(function($stateProvider) {
  $stateProvider.
  state('report', {
    url: '/report',
    templateUrl: 'app_components/report/report.html',
    controller: 'ReportController'
  });
});
