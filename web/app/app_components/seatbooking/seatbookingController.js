'use strict';

var app = angular.module('seatbooking', ['seatbooking.service', 'seatbooking.routes', 'train.service', 'ui.router', 'ui.bootstrap', 'ui.select', 'page-header', 'dialogs', 'underscore', 'angular-growl', 'xtForm', 'sdeutils', 'app.config', 'app.constants']);

app.controller('SeatbookingController', function(seatbookingService, trainService, $scope, $filter, $dialogs, $state, _, growl, sdeutils, APP_CONFIG) {

  // Clear the form and make a REST call to populate the list of jobs and other form lookup elements
  function init() {
    $scope.clearForm(false);
    initializeTrains();
  }

  function initializeTrains() {
    // REST call to load list of classifiers
    trainService.findAll({
      'size': 1000
    }).then(function(trains) {
      $scope.trainList = trains;
    }, function() {
      growl.error('Could not load train list.');
    });
  }

  // Clear the form
  $scope.clearForm = function(showForm) {
    $scope.model = {
      show: showForm,
      selectedFile: '',
      train: ''
    };
    $scope.fileInput = '';
    if (angular.isDefined($scope.upload_form)) {
      $scope.upload_form.$setPristine();
    }
  };

  $scope.showForm = function() {
    $scope.model.show = true;
  };

  $scope.onFileSelect = function($files) {

    // File name is valid, now running additional checks
    if ($files[0].size === 0) {
      growl.error('File should not be empty', {
        ttl: 4000
      });
    } else if ($files[0].size > APP_CONFIG.maxFileSize) {
      growl.error('File Size should not be greater than 100 MB', {
        ttl: 4000,
        inline: true
      });
    } else {
      $scope.model.selectedFile = $files[0];
      return;
    }
    $scope.fileInput = '';
  };

  $scope.onClickUpload = function() {

    $scope.model.isFileUploading = true;
    growl.info('Uploading in progress. The file ' + $scope.model.selectedFile.name + ' is being uploaded.', {
      ttl: 4000
    });

    // Populate data object
    var data = {};
    if ($scope.model.train) {
      data.id = $scope.model.train.getId();
    }

    seatbookingService.uploadFile(data, $scope.model.selectedFile).then(function() {
      // Success
      growl.success('Upload complete. The file has been uploaded successfully.', {
        ttl: 6000
      });

      $scope.model.isFileUploading = false;

    }, function(response) {
      // Error
      growl.error(response.data ? response.data.message : 'Error');
      $scope.model.isFileUploading = false;
    });

    $scope.clearForm(false);
  };

  $scope.fileSize = sdeutils.humanReadableSize;

  // This is to prevent users from navigating away from page while file upload is in progress
  $scope.confirmOnStateChange = function(event, toState) {
    if ($scope.model.isFileUploading) {
      event.preventDefault();
      var confirmation = $dialogs.confirm('Confirm', 'File Upload is in progress. Do You wish to continue?');

      confirmation.result.then(function() {
        $scope.model.isFileUploading = false;
        $state.go(toState.name, {
          url: toState.Url
        });
      });
    }
  };
  $scope.$on('$stateChangeStart', $scope.confirmOnStateChange);

  init();

});
