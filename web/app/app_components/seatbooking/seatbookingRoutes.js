'use strict';

var app = angular.module('seatbooking.routes', ['ui.router']);

app.config(function($stateProvider) {
  $stateProvider.
  state('seatbooking', {
    url: '/seatbooking',
    templateUrl: 'app_components/seatbooking/seatbooking.html',
    controller: 'SeatbookingController'
  });
});
