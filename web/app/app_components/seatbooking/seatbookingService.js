'use strict';

var app = angular.module('seatbooking.service', ['restangular', 'seatbooking.model', 'service', 'angularFileUpload', 'app.config']);

app.factory('seatbookingService', function(Restangular, BaseService, Seatbooking, $upload, APP_CONFIG) {

  var uploadRoute = APP_CONFIG.baseUrl + '/seatBookings/upload';

  var SeatbookingService = BaseService.extend({

    init: function() {
      this._super(Seatbooking, Restangular);
    },

    findByCreatedById: function(userId) {
      return this.future(this.Restangular.all(this.route).customGETLIST('/search/findByCreatedById', {
        userId: userId,
        size: 1000
      }));
    },

    uploadFile: function(data, selectedFile) {

      return this.future($upload.upload({
        url: uploadRoute,
        method: 'POST',
        data: {
          train: data
        },
        formDataAppender: function(formData, key, val) {
          var content = JSON.stringify(val);
          var blob = new Blob([content], {
            type: 'application/json'
          });
          formData.append(key, blob);
        },
        file: selectedFile
      }));
    }
  });
  return new SeatbookingService();
});
