'use strict';

var app = angular.module('seatbooking.model', ['model']);

app.factory('Seatbooking', function(BaseModel) {

  var Seatbooking = BaseModel.extend({
    init: function(data) {
      this._super(data);
    },

    getId: function() {
      return (!this.isNew()) ? this._links.self.href.substring(this._links.self.href.lastIndexOf('/') + 1) : undefined;
    }

  });

  Seatbooking.route = 'seatBookings';

  return Seatbooking;
});
