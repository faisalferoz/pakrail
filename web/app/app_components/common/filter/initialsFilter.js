'use strict';

angular.module('app.filters', []).filter('initials', function() {
  return function(str) {
    return (str === undefined || str === null) ? '' : str
      .replace(/_|-/, ' ').replace(/\w\S*/g, function(txt) {
        return txt.charAt(0);
      }).replace(' ', '');
  };
});
