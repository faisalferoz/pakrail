'use strict';
angular.module('app', ['ui.router', 'ui.bootstrap', 'chieffancypants.loadingBar', 'Mac', 'ngAnimate', 'fx.animations', 'ngTable', 'seatbooking', 'report', 'footer', 'app.config', 'app.config.restangular']).config(function($stateProvider, $urlRouterProvider, cfpLoadingBarProvider) {

  cfpLoadingBarProvider.includeSpinner = false;

  // For any unmatched url, send to /job
  $urlRouterProvider.otherwise('/seatbooking');

}).run(function($rootScope, $state, $stateParams) {

  // will be removed when ui-router 3.0 is released
  $rootScope.$state = $state;
  $rootScope.$stateParams = $stateParams;
});
