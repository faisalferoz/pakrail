'use strict';

angular.module('sdeutils', [])
  .service('sdeutils', function() {

    // This method can be used to get inner properties of any object. Angular JS only allows 1st level childen to be accessed using the obj['childProp'] notation
    // Warning, this does not work on arrays
    this.getDescendantProp = function(obj, desc) {
      if (desc === null || desc === undefined || desc.length === 0) {
        return obj;
      }
      var arr = desc.split('.');
      while (arr.length) {
        if (obj === null || obj === undefined) {
          return undefined;
        }
        obj = obj[arr.shift()];
      }
      return obj;
    };

    // This method will pretty print file sizes (1.7 kB instead of 1742 bytes)
    this.humanReadableSize = function(fileSizeInBytes) {
      if (fileSizeInBytes === null || fileSizeInBytes === undefined) {
        // FIXME: Should this be null or undefined?
        return null;
      }

      if (fileSizeInBytes < 103) {
        return fileSizeInBytes + ' bytes';
      }

      var i = -1;
      var byteUnits = [' kB', ' MB', ' GB', ' TB', 'PB', 'EB', 'ZB', 'YB'];
      do {
        fileSizeInBytes = fileSizeInBytes / 1024;
        i++;
      } while (fileSizeInBytes > 1024);
      return Math.max(fileSizeInBytes, 0.1).toFixed(1) + byteUnits[i];

    };

    // This method will remove any keys from the given object which have null, undefined or empty values
    // We require for filters to work properly on columns which have blank values
    this.clearFilter = function(filter) {
      if (filter) {
        angular.forEach(filter, function(value, key) {
          if (value === '' || value === undefined || typeof(value) === undefined || value === null) {
            delete filter[key];
          }
        });
      }
    };
  });
