"use strict";

describe("BaseService", function() {
  var BaseModel;
  var ExtendedModel;
  var BaseService;
  var ExtendedService;
  var service;
  var Restangular;
  var httpBackend;
  var appConfig;

  beforeEach(module("app.config.restangular"));
  beforeEach(module("model"));
  beforeEach(module("service"));

  beforeEach(inject(function(_BaseModel_, _BaseService_, _Restangular_, $httpBackend, APP_CONFIG) {
    BaseModel = _BaseModel_;
    BaseService = _BaseService_;
    Restangular = _Restangular_;
    httpBackend = $httpBackend;
    appConfig = APP_CONFIG;

    ExtendedModel = BaseModel.extend({
      init: function(data) {
        this._super(data);
      },

      isNew: function() {
        return !(this._links && this._links.self);
      }
    });

    ExtendedModel.route = "route";

    var ExtendedService = BaseService.extend({
      init: function() {
        this._super(ExtendedModel, Restangular);
      },
      //Add any exteded behavior here
      findAll: function() {
        return this.future(this.Restangular.all(this.Model.route).getList());
      }
    });

    service = new ExtendedService(ExtendedModel, Restangular);

  }));


  afterEach(function() {
    httpBackend.verifyNoOutstandingExpectation();
    httpBackend.verifyNoOutstandingRequest();
  });



  it("Should call extended function isNew", function() {
    var model = new ExtendedModel();
    expect(model.isNew()).toBe(true);

    var savedModel;
    service.save(model).then(function(data) {
      savedModel = data;
    });

    var responseObject = {
      "cmsParameterId": 1,
      "_links": {
        "self": {
          "href": appConfig.baseUrl + "/route/53442031e4b0bbd6adee7e30"
        }
      }
    };
    httpBackend.when('POST', appConfig.baseUrl + '/route').respond(responseObject);
    httpBackend.flush();

    expect(savedModel.isNew()).toBe(false);
  });




  it("Should get result and extend the returned object", function() {
    var model = new ExtendedModel();
    expect(model.isNew()).toBe(true);

    var selfLink = appConfig.baseUrl + "/domainKnowledges/53442031e4b0bbd6adee7e30";

    var returnedModel;
    service.findOne(selfLink).then(function(data) {
      returnedModel = data;
    });

    var responseObject = {
      "cmsParameterId": 1,
      "_links": {
        "self": {
          "href": selfLink
        }
      }
    };
    httpBackend.when('GET', selfLink).respond(responseObject);
    httpBackend.flush();

    expect(returnedModel.isNew()).toBe(false);
  });


  it("Should update object", function() {
    var selfLink = appConfig.baseUrl + "/route/53442031e4b0bbd6adee7e30";
    var obj = {
      "cmsParameterId": 1,
      "_links": {
        "self": {
          "href": selfLink
        }
      }
    };

    var model = new ExtendedModel(obj);

    var savedModel;
    service.save(model).then(function(data) {
      savedModel = data;
    });

    httpBackend.when('PUT', selfLink).respond(obj);
    httpBackend.flush();

    expect(savedModel.isNew()).toBe(false);
  });


  it("Should remove object", function() {
    var selfLink = appConfig.baseUrl + "/route/53442031e4b0bbd6adee7e30";
    var obj = {
      "cmsParameterId": 1,
      "_links": {
        "self": {
          "href": selfLink
        }
      }
    };

    var model = new ExtendedModel(obj);
    service.remove(model);

    httpBackend.when('DELETE', selfLink).respond({});
    httpBackend.flush();
  });

  it("Should get list of all objects", function() {
    var allLinks = appConfig.baseUrl + "/dataSets?size=1000";

    var listData = {
      "_links": {
        "self": {
          "href": appConfig.baseUrl + "/dataSets?size=1000{&page,sort}",
          "templated": true
        },
        "upload": {
          "href": appConfig.baseUrl + "/dataSet/upload"
        }
      },
      "page": {
        "size": 1000,
        "totalElements": 0,
        "totalPages": 0,
        "number": 0
      }
    };

    var DatasetTestModel = BaseModel.extend({
      init: function(data) {
        this._super(data);
      }
    });

    DatasetTestModel.route = "dataSets";

    var DatasetTestService = BaseService.extend({
      init: function() {
        this._super(DatasetTestModel, Restangular);
      }
    });

    service = new DatasetTestService(DatasetTestModel, Restangular);

    service.findAll({
      size: 1000
    });

    httpBackend.when('GET', allLinks).respond(listData);

    httpBackend.flush();

  });
});
