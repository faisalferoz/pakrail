'use strict';

describe('initials', function() {

  var fixtures = [
    'AWAITING DISPATCH',
    'AWAITING_DISPATCH',
    'AWaiTing-DISPatcH',
    'Awaiting DISPATCH'
  ];

  beforeEach(module('app.filters'));

  it('should convert strings correctly', inject(function(initialsFilter) {

    fixtures.forEach(function(fixture) {
      expect(initialsFilter(fixture)).toEqual('AD');
    });

  }));

  it('should return an empty string when a value is not passed', inject(function(initialsFilter) {
    expect(initialsFilter()).toEqual('');
    expect(initialsFilter(null)).toEqual('');
  }));
});
