"use strict";

describe("BaseModel", function() {
  var BaseModel;
  var ExtendedModel;
  var model;

  beforeEach(module("model"));

  beforeEach(inject(function(_BaseModel_) {
    BaseModel = _BaseModel_;
    ExtendedModel = BaseModel.extend({
      init: function(data) {
        this._super(data);
      },

      myFn: function() {
        return !this.myProp;
      },
      something: 'Aqeela Hemani'
    });

    model = new ExtendedModel({
      myProp: 1
    });
  }));

  it("Should call extended function myFn", function() {
    expect(model.myFn()).toBe(false);
  });

  it("Should access extended property myProp", function() {
    expect(model.myProp).toBe(1);
  });

  it("Should call base function isNew", function() {
    expect(model.isNew()).toBe(true);
  });

});
