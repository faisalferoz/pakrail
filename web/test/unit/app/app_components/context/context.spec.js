"use strict";

describe("context", function() {
  var context;
  var rootScope;

  beforeEach(module("context.holder"));

  beforeEach(inject(function(_context_, $rootScope) {
    context = _context_;
    rootScope = $rootScope;
    spyOn(rootScope, '$broadcast');
  }));

  it("Context should be empty when initialized", function() {
    expect(context.getCurrent()).toBe(null);
  });

  it("Context should be set and event broadcasted", function() {
    var obj = {
      something: 'something'
    };
    context.setCurrent(obj);
    expect(context.getCurrent()).toBe(obj);
    expect(rootScope.$broadcast).toHaveBeenCalledWith(context.EVENT_UPDATE, obj);
  });

  it("Context should be cleared and event broadcasted", function() {
    context.clear();
    expect(context.getCurrent()).toBe(null);
    expect(rootScope.$broadcast).toHaveBeenCalledWith(context.EVENT_CLEAR);
  });

});
