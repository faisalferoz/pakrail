
package com.pakrail.core.train;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * Ticket code repository
 *
 * @author Faisal Feroz
 *
 */
public interface TicketCodeRepository extends JpaRepository<TicketCode, String> {

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.data.repository.CrudRepository#save(java.lang.Iterable)
     */
    @RestResource(exported = false)
    @Override
    public <S extends TicketCode> List<S> save(Iterable<S> entities);

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.data.repository.CrudRepository#save(S)
     */
    @RestResource(exported = false)
    @Override
    public <S extends TicketCode> S save(S entity);

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.data.repository.CrudRepository#delete(java.io.Serializable)
     */
    @RestResource(exported = false)
    @Override
    public void delete(String id);

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.data.repository.CrudRepository#delete(java.lang.Object)
     */
    @RestResource(exported = false)
    @Override
    public void delete(TicketCode entity);
}
