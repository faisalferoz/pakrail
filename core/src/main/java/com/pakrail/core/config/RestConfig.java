
package com.pakrail.core.config;

import javax.servlet.Filter;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;
import org.springframework.validation.Validator;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import com.pakrail.core.rest.filter.SimpleCORSFilter;
import com.pakrail.core.train.SeatBookingEventHandler;
import com.pakrail.core.train.Station;
import com.pakrail.core.train.TicketCode;

@Configuration
public class RestConfig extends RepositoryRestMvcConfiguration {

    @Bean
    public Filter simpleCORSFilter() {
        return new SimpleCORSFilter();
    }

    @Bean
    public Validator jsr303Validator() {
        return new LocalValidatorFactoryBean();
    }

    @Bean
    public SeatBookingEventHandler seatBookingEventHandler() {
        return new SeatBookingEventHandler();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration#
     * configureRepositoryRestConfiguration
     * (org.springframework.data.rest.core.config.RepositoryRestConfiguration)
     */
    @Override
    protected void configureRepositoryRestConfiguration(
            final RepositoryRestConfiguration config) {
        config.setReturnBodyOnCreate(true);
        config.setReturnBodyOnUpdate(true);
        config.exposeIdsFor(Station.class, TicketCode.class);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration#
     * configureValidatingRepositoryEventListener
     * (org.springframework.data.rest.core.event.ValidatingRepositoryEventListener)
     */
    @Override
    protected void configureValidatingRepositoryEventListener(
            ValidatingRepositoryEventListener validatingListener) {
        validatingListener.addValidator("beforeCreate", jsr303Validator());
        validatingListener.addValidator("beforeSave", jsr303Validator());
    }
}
