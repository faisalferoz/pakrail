
package com.pakrail.core.train;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * Train repository
 *
 * @author Faisal Feroz
 *
 */
public interface TrainRepository extends JpaRepository<Train, Integer> {

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.data.repository.CrudRepository#save(java.lang.Iterable)
     */
    @RestResource(exported = false)
    @Override
    public <S extends Train> List<S> save(Iterable<S> entities);

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.data.repository.CrudRepository#save(S)
     */
    @RestResource(exported = false)
    @Override
    public <S extends Train> S save(S entity);

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.data.repository.CrudRepository#delete(java.io.Serializable)
     */
    @RestResource(exported = false)
    @Override
    public void delete(Integer id);

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.data.repository.CrudRepository#delete(java.lang.Object)
     */
    @RestResource(exported = false)
    @Override
    public void delete(Train entity);
}
