
package com.pakrail.core.train;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.joda.time.DateTime;

import com.pakrail.core.base.AbstractJPAEntity;

@Entity
public class SeatBooking extends AbstractJPAEntity<Integer> implements Serializable {

    private static final long serialVersionUID = 2323382746646747648L;

    @Temporal(TemporalType.DATE)
    @Column(name = "departure_date", nullable = false)
    private Date departureDate;

    @Column(name = "passenger_name", length = 255, nullable = false)
    private String passengerName;

    @Column(name = "passenger_cnic", length = 15, nullable = false)
    private String passengerCnic;

    @Column(name = "passenger_mobile", length = 11)
    private String passengerMobile;

    // @ManyToOne(fetch = FetchType.EAGER, optional = true)
    // @JoinColumn(name = "seat_plan_id", referencedColumnName = "id", nullable = true)
    private Integer seatPlanId;

    // @ManyToOne(fetch = FetchType.EAGER, optional = true)
    // @JoinColumn(name = "fare_id", referencedColumnName = "id", nullable = true)
    private Integer fareId;

    // @ManyToOne(fetch = FetchType.EAGER, optional = false)
    // @JoinColumn(name = "created_by_id", referencedColumnName = "id", nullable = false)
    private Integer createdById;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    private boolean penalty = false;

    private boolean hasGuardCertificate = false;

    private Integer evm;

    private Integer amount;

    @Column(name = "luggage_amount")
    private Integer luggageAmount;

    @Column(name = "is_cancelled", nullable = false, columnDefinition = "bit(1) default 0")
    private boolean isCancelled = false;

    @Column(name = "pct_no", length = 255, nullable = false)
    private String pctNo;

    public DateTime getCreatedDate() {
        return null == createdDate ? null : new DateTime(createdDate);
    }

    public void setCreatedDate(final DateTime createdDate) {
        this.createdDate = null == createdDate ? null : createdDate.toDate();
    }

    public DateTime getDepartureDate() {
        return null == departureDate ? null : new DateTime(departureDate);
    }

    public void setDepartureDate(final DateTime departureDate) {
        this.departureDate = null == departureDate ? null : departureDate.toDate();
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public String getPassengerCnic() {
        return passengerCnic;
    }

    public void setPassengerCnic(String passengerCnic) {
        this.passengerCnic = passengerCnic;
    }

    public String getPassengerMobile() {
        return passengerMobile;
    }

    public void setPassengerMobile(String passengerMobile) {
        this.passengerMobile = passengerMobile;
    }

    public Integer getSeatPlanId() {
        return seatPlanId;
    }

    public void setSeatPlanId(Integer seatPlanId) {
        this.seatPlanId = seatPlanId;
    }

    public Integer getFareId() {
        return fareId;
    }

    public void setFareId(Integer fareId) {
        this.fareId = fareId;
    }

    public Integer getCreatedById() {
        return createdById;
    }

    public void setCreatedById(Integer createdById) {
        this.createdById = createdById;
    }

    public boolean getPenalty() {
        return penalty;
    }

    public void setPenalty(boolean penalty) {
        this.penalty = penalty;
    }

    public Integer getEvm() {
        return evm;
    }

    public void setEvm(Integer evm) {
        this.evm = evm;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getPctNo() {
        return pctNo;
    }

    public void setPctNo(String pctNo) {
        this.pctNo = pctNo;
    }

    public boolean isHasGuardCertificate() {
        return hasGuardCertificate;
    }

    public void setHasGuardCertificate(boolean hasGuardCertificate) {
        this.hasGuardCertificate = hasGuardCertificate;
    }

    public Integer getLuggageAmount() {
        return luggageAmount;
    }

    public void setLuggageAmount(Integer luggageAmount) {
        this.luggageAmount = luggageAmount;
    }

    public boolean isCancelled() {
        return isCancelled;
    }

    public void setCancelled(boolean isCancelled) {
        this.isCancelled = isCancelled;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SeatBooking)) {
            return false;
        }

        final SeatBooking booking = (SeatBooking) obj;

        return new EqualsBuilder() //
        .append(getId(), booking.getId()) //
        .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder() //
        .append(getId()) //
        .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this) //
        .append("Id", getId()) //
        .append("PCT No", pctNo) //
        .append("Departure Date", departureDate) //
        .append("Penalty", penalty) //
        .append("Seat Plan Id", seatPlanId) //
        .toString();
    }

}
