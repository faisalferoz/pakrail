
package com.pakrail.core.train;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

/**
 *
 * @author Faisal Feroz
 *
 */
@Entity
public class TicketCode implements Serializable {

    private static final long serialVersionUID = 6523917718061192108L;

    @Id
    private String id;

    @Column(name = "description", length = 100, nullable = false)
    private String description;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public final boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof TicketCode)) {
            return false;
        }

        final TicketCode model = (TicketCode) obj;

        return new EqualsBuilder() //
        .append(getId(), model.getId()) //
        .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder() //
        .append(getId()) //
        .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this) //
        .append("Id", getId()) //
        .append("Description", description) //
        .toString();
    }
}
