
package com.pakrail.core.train;

import org.joda.time.DateTime;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;

@RepositoryEventHandler(SeatBooking.class)
public class SeatBookingEventHandler {

    @HandleBeforeCreate
    public void handleBeforeCreate(final SeatBooking seatBooking) {
        seatBooking.setCreatedDate(new DateTime());
    }
}
