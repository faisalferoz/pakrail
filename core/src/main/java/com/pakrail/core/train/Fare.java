
package com.pakrail.core.train;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.pakrail.core.base.AbstractJPAEntity;

@Entity
public class Fare extends AbstractJPAEntity<Integer> implements Serializable {

    private static final long serialVersionUID = -5762916730072725834L;

    @Column(name = "class_code", length = 10, nullable = false)
    private String classCode;

    @Column(name = "fare", nullable = false)
    private Integer fare;

    @Column(name = "excise_duty", nullable = false)
    private Integer exciseDuty;

    @Column(name = "start_date", nullable = false)
    private Date startDate;

    @Column(name = "end_date", nullable = false)
    private Date endDate;

    @Column(name = "alternate_fare", nullable = false)
    private Integer alternateFare;

    @Column(name = "alternate_excise_duty", nullable = false)
    private Integer alternateExciseDuty;

    @Column(name = "distance", nullable = false)
    private Integer distance;

    @Column(name = "remarks", length = 255)
    private String remarks;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "station_to_id", referencedColumnName = "id", nullable = false)
    private Station stationToId;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "station_from_id", referencedColumnName = "id", nullable = false)
    private Station stationFromId;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "ticket_code_id", referencedColumnName = "id", nullable = false)
    private TicketCode ticketCode;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "train_id", referencedColumnName = "id", nullable = false)
    private Train train;

    public String getClassCode() {
        return classCode;
    }

    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    public Integer getFare() {
        return fare;
    }

    public void setFare(Integer fare) {
        this.fare = fare;
    }

    public Integer getExciseDuty() {
        return exciseDuty;
    }

    public void setExciseDuty(Integer exciseDuty) {
        this.exciseDuty = exciseDuty;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getAlternateFare() {
        return alternateFare;
    }

    public void setAlternateFare(Integer alternateFare) {
        this.alternateFare = alternateFare;
    }

    public Integer getAlternateExciseDuty() {
        return alternateExciseDuty;
    }

    public void setAlternateExciseDuty(Integer alternateExciseDuty) {
        this.alternateExciseDuty = alternateExciseDuty;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Station getStationToId() {
        return stationToId;
    }

    public void setStationToId(Station stationToId) {
        this.stationToId = stationToId;
    }

    public Station getStationFromId() {
        return stationFromId;
    }

    public void setStationFromId(Station stationFromId) {
        this.stationFromId = stationFromId;
    }

    public TicketCode getTicketCode() {
        return ticketCode;
    }

    public void setTicketCode(TicketCode ticketCode) {
        this.ticketCode = ticketCode;
    }

    public Train getTrain() {
        return train;
    }

    public void setTrain(Train train) {
        this.train = train;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Fare)) {
            return false;
        }

        final Fare model = (Fare) obj;

        return new EqualsBuilder() //
        .append(getId(), model.getId()) //
        .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder() //
        .append(getId()) //
        .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this) //
        .append("Id", getId()) //
        .append("Fare", fare) //
        .append("Train", train) //
        .toString();
    }
}
