
package com.pakrail.core.train;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.joda.time.DateTime;

import com.pakrail.core.base.AbstractJPAEntity;

@Entity
public class SeatBookingHistory extends AbstractJPAEntity<Integer> implements
        Serializable {

    private static final long serialVersionUID = 2323382746646747648L;

    @Temporal(TemporalType.DATE)
    @Column(name = "departure_date", nullable = false)
    private Date departureDate;

    @Column(name = "passenger_name", length = 255, nullable = false)
    private String passengerName;

    @Column(name = "passenger_cnic", length = 15, nullable = false)
    private String passengerCnic;

    @Column(name = "passenger_mobile", length = 11)
    private String passengerMobile;

    @ManyToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "seat_plan_id", referencedColumnName = "id", nullable = true)
    private SeatPlan seatPlan;

    @ManyToOne(fetch = FetchType.EAGER, optional = true)
    @JoinColumn(name = "fare_id", referencedColumnName = "id", nullable = true)
    private Fare fare;

    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "created_by_id", referencedColumnName = "id", nullable = false)
    private User createdBy;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    private boolean penalty = false;

    private boolean hasGuardCertificate = false;

    private Integer evm;

    private Integer amount;

    @Column(name = "luggage_amount")
    private Integer luggageAmount;

    @Column(name = "from_station")
    private String fromStation;

    @Column(name = "to_station")
    private String toStation;

    @Column(name = "pct_no", length = 255, nullable = false)
    private String pctNo;

    @Column(name = "is_cancelled", nullable = false, columnDefinition = "bit(1) default 0")
    private boolean isCancelled = false;

    public DateTime getCreatedDate() {
        return null == createdDate ? null : new DateTime(createdDate);
    }

    public void setCreatedDate(final DateTime createdDate) {
        this.createdDate = null == createdDate ? null : createdDate.toDate();
    }

    public DateTime getDepartureDate() {
        return null == departureDate ? null : new DateTime(departureDate);
    }

    public void setDepartureDate(final DateTime departureDate) {
        this.departureDate = null == departureDate ? null : departureDate.toDate();
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public String getPassengerCnic() {
        return passengerCnic;
    }

    public void setPassengerCnic(String passengerCnic) {
        this.passengerCnic = passengerCnic;
    }

    public String getPassengerMobile() {
        return passengerMobile;
    }

    public void setPassengerMobile(String passengerMobile) {
        this.passengerMobile = passengerMobile;
    }

    public SeatPlan getSeatPlan() {
        return seatPlan;
    }

    public void setSeatPlan(SeatPlan seatPlan) {
        this.seatPlan = seatPlan;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Fare getFare() {
        return fare;
    }

    public void setFare(Fare fare) {
        this.fare = fare;
    }

    public boolean getPenalty() {
        return penalty;
    }

    public void setPenalty(boolean penalty) {
        this.penalty = penalty;
    }

    public Integer getEvm() {
        return evm;
    }

    public void setEvm(Integer evm) {
        this.evm = evm;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getPctNo() {
        return pctNo;
    }

    public void setPctNo(String pctNo) {
        this.pctNo = pctNo;
    }

    public boolean isHasGuardCertificate() {
        return hasGuardCertificate;
    }

    public void setHasGuardCertificate(boolean hasGuardCertificate) {
        this.hasGuardCertificate = hasGuardCertificate;
    }

    public Integer getLuggageAmount() {
        return luggageAmount;
    }

    public void setLuggageAmount(Integer luggageAmount) {
        this.luggageAmount = luggageAmount;
    }

    public boolean isCancelled() {
        return isCancelled;
    }

    public void setCancelled(boolean isCancelled) {
        this.isCancelled = isCancelled;
    }

    public String getFromStation() {
        return fromStation;
    }

    public void setFromStation(String fromStation) {
        this.fromStation = fromStation;
    }

    public String getToStation() {
        return toStation;
    }

    public void setToStation(String toStation) {
        this.toStation = toStation;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SeatBookingHistory)) {
            return false;
        }

        final SeatBookingHistory booking = (SeatBookingHistory) obj;

        return new EqualsBuilder() //
        .append(getId(), booking.getId()) //
        .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder() //
        .append(getId()) //
        .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this) //
        .append("Id", getId()) //
        .append("PCT No", pctNo) //
        .append("Departure Date", departureDate) //
        .append("Penalty", penalty) //
        .append("Seat Plan", seatPlan) //
        .toString();
    }

}
