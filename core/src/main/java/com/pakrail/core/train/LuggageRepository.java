
package com.pakrail.core.train;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * Luggage repository
 *
 * @author Faisal Feroz
 *
 */
public interface LuggageRepository extends JpaRepository<Luggage, Integer> {

    /**
     * Returns all {@link Luggage} who are eligible after the given date
     *
     * @param date
     * @return
     */
    @Query("FROM Luggage WHERE CURRENT_TIMESTAMP BETWEEN startDate AND endDate")
    Iterable<Luggage> findApplicableLuggages();

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.data.repository.CrudRepository#save(java.lang.Iterable)
     */
    @RestResource(exported = false)
    @Override
    public <S extends Luggage> List<S> save(Iterable<S> entities);

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.data.repository.CrudRepository#save(S)
     */
    @RestResource(exported = false)
    @Override
    public <S extends Luggage> S save(S entity);

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.data.repository.CrudRepository#delete(java.io.Serializable)
     */
    @RestResource(exported = false)
    @Override
    public void delete(Integer id);

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.data.repository.CrudRepository#delete(java.lang.Object)
     */
    @RestResource(exported = false)
    @Override
    public void delete(Luggage entity);
}
