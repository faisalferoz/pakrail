
package com.pakrail.core.train;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.pakrail.core.base.AbstractJPAEntity;

/**
 * Represents the Departure domain object
 *
 * @author Faisal Feroz
 *
 */
@Entity
public class Departure extends AbstractJPAEntity<Integer> {

    private static final long serialVersionUID = 8755550444266656898L;

    @OneToOne(fetch = FetchType.EAGER, optional = false)
    private Train train;

    @Temporal(TemporalType.DATE)
    @Column(name = "departure_date", nullable = false)
    private Date departureDate;

    public Train getTrain() {
        return train;
    }

    public void setTrain(Train train) {
        this.train = train;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(final Date departureDate) {
        this.departureDate = departureDate;
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Departure)) {
            return false;
        }

        final Departure departure = (Departure) obj;

        return new EqualsBuilder() //
        .append(train, departure.getTrain()) //
        .append(departureDate, departure.getDepartureDate()) //
        .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder() //
        .append(train) //
        .append(departureDate) //
        .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this) //
        .append("Id", getId()) //
        .append("Train Id", train.getId()) //
        .append("Departure Date", departureDate) //
        .toString();
    }

}
