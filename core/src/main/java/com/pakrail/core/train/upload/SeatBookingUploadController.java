/*
 * #region
 * core
 * %%
 * Copyright (C) 2013 - 2014 Etilize
 * %%
 * NOTICE: All information contained herein is, and remains the property of ETILIZE.
 * The intellectual and technical concepts contained herein are proprietary to
 * ETILIZE and may be covered by U.S. and Foreign Patents, patents in process, and
 * are protected by trade secret or copyright law. Dissemination of this information
 * or reproduction of this material is strictly forbidden unless prior written
 * permission is obtained from ETILIZE. Access to the source code contained herein
 * is hereby forbidden to anyone except current ETILIZE employees, managers or
 * contractors who have executed Confidentiality and Non-disclosure agreements
 * explicitly covering such access.
 *
 * The copyright notice above does not evidence any actual or intended publication
 * or disclosure of this source code, which includes information that is confidential
 * and/or proprietary, and is a trade secret, of ETILIZE. ANY REPRODUCTION, MODIFICATION,
 * DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS
 * SOURCE CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF ETILIZE IS STRICTLY PROHIBITED,
 * AND IN VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES. THE RECEIPT
 * OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR
 * IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO
 * MANUFACTURE, USE, OR SELL ANYTHING THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
 * #endregion
 */

package com.pakrail.core.train.upload;

import static au.com.bytecode.opencsv.CSVReader.*;

import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ControllerUtils;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import au.com.bytecode.opencsv.CSVReader;

import com.pakrail.core.train.Departure;
import com.pakrail.core.train.DepartureRepository;
import com.pakrail.core.train.Fare;
import com.pakrail.core.train.FareRepository;
import com.pakrail.core.train.SeatBooking;
import com.pakrail.core.train.SeatBookingRepository;
import com.pakrail.core.train.SeatPlan;
import com.pakrail.core.train.SeatPlanRepository;
import com.pakrail.core.train.Train;
import com.pakrail.core.train.User;

/**
 * Controller for uploading data set
 *
 * @author Faisal Feroz
 *
 */
@Controller
@RequestMapping("/seatBookings/upload")
public class SeatBookingUploadController {

    final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private DepartureRepository departureRepository;

    @Autowired
    private SeatBookingRepository seatBookingRepository;

    @Autowired
    private SeatPlanRepository seatPlanRepository;

    @Autowired
    private FareRepository fareRepository;

    /**
     * The method exposes rest interface for uploading file data to SDE. It expects the
     * file to upload along with any comments and source from which the file was obtained
     * as meta data. The file is stored to path specified as feedFilePath specified in
     * application.properties
     *
     * @param dataSet
     * @param file
     * @return
     * @throws IOException
     * @throws Exception
     */
    @RequestMapping(method = RequestMethod.POST, produces = { "application/json",
        "text/uri-list" })
    public HttpEntity<ResourceSupport> upload(@RequestPart final Train train,
            final MultipartFile file) throws Exception {

        Assert.notNull(train);
        Assert.notNull(train.getId());

        final SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        Date departureDate = null;

        // skip header
        try (final CSVReader reader = new CSVReader(new InputStreamReader(
                file.getInputStream()), DEFAULT_SEPARATOR, DEFAULT_QUOTE_CHARACTER, 1)) {

            logger.info("Importing bookings from file {}", file.getOriginalFilename());

            final List<SeatBooking> bookings = new ArrayList<>(50);
            final DateTime createdDate = new DateTime();
            final User createdBy = new User();
            createdBy.setId(1);

            for (final String[] line : reader.readAll()) {
                // first col is departure date
                departureDate = format.parse(line[0]);

                final String gFlags = line[8];
                // if the seat is booked
                if (StringUtils.hasText(gFlags) && gFlags.equalsIgnoreCase("F")) {
                    final int amount = Integer.parseInt(line[21]);

                    final SeatBooking booking = new SeatBooking();
                    booking.setPassengerName(line[9].trim());
                    booking.setPctNo(line[10].trim());
                    booking.setDepartureDate(new DateTime(departureDate));
                    booking.setPassengerCnic(line[25].trim());
                    booking.setAmount(amount);
                    booking.setCreatedById(createdBy.getId());
                    booking.setCreatedDate(createdDate);

                    // find seat plan
                    final int coach = Integer.parseInt(line[1]);
                    final String seatCode = line[2];
                    final String classCode = line[3].trim();
                    final String quota = line[5];
                    final String pFromStation = line[6];
                    final String pToStation = line[7];
                    final String ticketCode = line[20];

                    // find seat plan
                    final SeatPlan seatPlan = seatPlanRepository.findByTrainIdAndSeatCodeAndFromStationIdAndToStationIdAndQuotaAndCoach(
                            train.getId(), seatCode, pFromStation, pToStation, quota,
                            coach);
                    if (seatPlan != null) {
                        booking.setSeatPlanId(seatPlan.getId());
                    }

                    final Fare fare = fareRepository.findApplicableFareWithTrainIdAndTicketCodeAndFromStationIdAndToStationIdAndClassCode(
                            train.getId(), ticketCode, pFromStation, pToStation,
                            classCode);
                    if (fare != null) {
                        booking.setFareId(fare.getId());
                    }

                    bookings.add(booking);
                }
                // final String clazz = line[3];
                // final String toStation = line[4];
                // final String quota = line[5];
                // final String available = line[8];
                // final String passengerName = line[9];
                // final String pctNo = line[10];
                // final String vrNo = line[11];
                // final String compNo = line[12];
                // final String username = line[13];
                // final String remarks = line[14];
                // final String fromStation = line[15];
                // final String resDate = line[16];
                // final String resTime = line[17];
                // final Integer termCode = StringUtils.isEmpty(line[18]) ? null
                // : Integer.parseInt(line[18]);
                // final String ticketCode = line[20];
                // final Integer fare = StringUtils.isEmpty(line[21]) ? null
                // : Integer.parseInt(line[21]);
                // final String cninc = line[25];
            }

            Departure departure = departureRepository.findOneByTrainId(train.getId());
            if (departure == null) {
                departure = new Departure();
                departure.setTrain(train);
            }
            departure.setDepartureDate(departureDate);
            departureRepository.save(departure);

            if (!bookings.isEmpty()) {
                seatBookingRepository.save(bookings);
                logger.info("Import Completed");
            } else {
                logger.info("No Bookings in file {}", file.getOriginalFilename());
            }
        }
        return ControllerUtils.toEmptyResponse(HttpStatus.CREATED);
    }
}
