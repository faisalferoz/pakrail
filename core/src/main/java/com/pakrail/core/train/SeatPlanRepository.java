
package com.pakrail.core.train;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface SeatPlanRepository extends JpaRepository<SeatPlan, Integer> {

    Iterable<SeatPlan> findByTrainId(@Param("trainId") final Integer trainId);

    SeatPlan findByTrainIdAndSeatCodeAndFromStationIdAndToStationIdAndQuotaAndCoach(
            @Param("trainId") final Integer trainId,
            @Param("seatCode") final String seatCode,
            @Param("fromStationId") final String fromStationId,
            @Param("toStationId") final String toStationId,
            @Param("quota") final String quota, @Param("coach") final Integer coach);
}
