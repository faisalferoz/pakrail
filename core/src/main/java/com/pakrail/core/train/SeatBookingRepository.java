
package com.pakrail.core.train;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

/**
 * Seat booking repository
 *
 * @author Faisal Feroz
 *
 */
public interface SeatBookingRepository extends JpaRepository<SeatBooking, Integer> {

    @Query("FROM SeatBooking WHERE createdById != :userId AND createdDate >= :syncDate")
    List<SeatBooking> findLastChanged(@Param("userId") final Integer userId,
            @Param("syncDate") @DateTimeFormat(iso = ISO.DATE_TIME) final Date syncDate);

    Page<SeatBooking> findByCreatedById(@Param("userId") final Integer userId,
            final Pageable pageable);
}
