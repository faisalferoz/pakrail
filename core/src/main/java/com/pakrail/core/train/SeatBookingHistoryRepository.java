
package com.pakrail.core.train;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

/**
 * Seat booking history repository
 *
 * @author Faisal Feroz
 *
 */
public interface SeatBookingHistoryRepository extends
        JpaRepository<SeatBookingHistory, Integer> {

    @Query("FROM SeatBookingHistory WHERE createdBy.id != :userId AND createdDate >= :syncDate")
    List<SeatBookingHistory> findLastChanged(@Param("userId") final Integer userId,
            @Param("syncDate") @DateTimeFormat(iso = ISO.DATE_TIME) final Date syncDate);

    Page<SeatBookingHistory> findByCreatedById(@Param("userId") final Integer userId,
            final Pageable pageable);
}
