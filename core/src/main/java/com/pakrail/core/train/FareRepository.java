
package com.pakrail.core.train;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * Fare repository
 *
 * @author Faisal Feroz
 *
 */
public interface FareRepository extends JpaRepository<Fare, Integer> {

    /**
     * Returns all {@linkp Fare} who are eligible after the given date
     *
     * @param date
     * @return
     */
    @Query("FROM Fare WHERE CURRENT_TIMESTAMP BETWEEN startDate AND endDate")
    Iterable<Fare> findApplicableFares();

    @Query("FROM Fare WHERE CURRENT_TIMESTAMP BETWEEN startDate AND endDate "
            + "AND train.id=:trainId AND ticketCode.id=:ticketCode "
            + "AND stationToId.id=:toStationId AND stationFromId.id=:fromStationId AND classCode=:classCode")
    Fare findApplicableFareWithTrainIdAndTicketCodeAndFromStationIdAndToStationIdAndClassCode(
            @Param("trainId") final Integer trainId,
            @Param("ticketCode") final String ticketCode,
            @Param("fromStationId") final String fromStationId,
            @Param("toStationId") final String toStationId,
            @Param("classCode") final String classCode);

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.data.repository.CrudRepository#save(java.lang.Iterable)
     */
    @RestResource(exported = false)
    @Override
    public <S extends Fare> List<S> save(Iterable<S> entities);

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.data.repository.CrudRepository#save(S)
     */
    @RestResource(exported = false)
    @Override
    public <S extends Fare> S save(S entity);

    /*
     * (non-Javadoc)
     *
     * @see
     * org.springframework.data.repository.CrudRepository#delete(java.io.Serializable)
     */
    @RestResource(exported = false)
    @Override
    public void delete(Integer id);

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.data.repository.CrudRepository#delete(java.lang.Object)
     */
    @RestResource(exported = false)
    @Override
    public void delete(Fare entity);
}
