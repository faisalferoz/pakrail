
package com.pakrail.core.train;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.pakrail.core.base.AbstractJPAEntity;

@Entity
public class Train extends AbstractJPAEntity<Integer> implements Serializable {

    private static final long serialVersionUID = 1226944232608121018L;

    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "train", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<SeatPlan> seats = new ArrayList<SeatPlan>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SeatPlan> getSeats() {
        return seats;
    }

    public void setSeats(List<SeatPlan> seats) {
        this.seats = seats;
    }

    @Override
    public final boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Train)) {
            return false;
        }

        final Train model = (Train) obj;

        return new EqualsBuilder() //
        .append(name, model.getName()) //
        .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder() //
        .append(name) //
        .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this) //
        .append("Id", getId()) //
        .append("Code", name) //
        .toString();
    }
}
