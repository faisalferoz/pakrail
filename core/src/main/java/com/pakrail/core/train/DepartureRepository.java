
package com.pakrail.core.train;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

/**
 * Represents the departure repository
 *
 * @author Faisal Feroz
 *
 */
public interface DepartureRepository extends JpaRepository<Departure, Integer> {

    /**
     * Returns the departure object by train id
     *
     * @param trainId
     * @return {@link Departure}
     */
    Departure findOneByTrainId(@Param("trainId") final Integer trainId);
}
