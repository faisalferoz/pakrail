
package com.pakrail.core.train;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User, Integer> {

    User findByUsernameAndPassword(@Param("username") final String username,
            @Param("password") final String password);
}
